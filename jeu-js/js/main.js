import {initWorld} from './world.js';
import { eventSeconds } from './frame.js';


function initGame(){
    var bodies = initWorld();
    eventSeconds(bodies);
}

initGame();

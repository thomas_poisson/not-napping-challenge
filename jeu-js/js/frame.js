import { Events, engine, World, Body } from './matter.js'

export function eventSeconds(bodies){

     // an example of using beforeUpdate event on an engine
     Events.on(engine, 'beforeUpdate', function(event) {

        // apply random forces every 5 secs
        if (event.timestamp % 1000 < 50){
            Body.rotate(bodies.seconds, 1/60, {x: 400, y:300})

            if(bodies.seconds.angle >= 3 && bodies.seconds.angle % (2 * Math.PI) <= 0.001){
                console.log("yes");
                Body.rotate(bodies.minutes, 1/12, {x: 400, y:300})
                console.log(bodies.minutes.angle % (2 * Math.PI));
                if(bodies.minutes.angle >= 3 && bodies.minutes.angle % (2 * Math.PI) <= 0.1){
                    console.log("hour");
                    Body.rotate(bodies.hours, 1/12, {x: 400, y:300})
                }
            }
        }
            
    });
}
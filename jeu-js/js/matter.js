
export const Engine = Matter.Engine;
export const Render = Matter.Render;
export const Runner = Matter.Runner;
export const Composites = Matter.Composites;
export const Events = Matter.Events;
export const Common = Matter.Common;
export const World = Matter.World;
export const Bodies = Matter.Bodies;
export const Body = Matter.Body;
export const MouseConstraint = Matter.MouseConstraint;
export const Mouse = Matter.Mouse;

export let engine = Engine.create();
export let render = Render.create({
  element: document.body,
  engine: engine,
  options: {wireframes:false}
});
// Gestion de la gravité sur la zone du jeu
//engine.world.gravity.y = 0;

Engine.run(engine);
Render.run(render);
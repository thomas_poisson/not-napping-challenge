import { engine, Bodies, World, render, Render, Body } from './matter.js'

export function initWorld(){
    let seconds = Bodies.rectangle(400,200,10,250,{ isStatic: true});
    let minutes = Bodies.rectangle(400,200,50,200,{ isStatic: true}); 
    Body.setCentre(minutes, {x: 400, y:300});
    Body.setAngle(minutes, 6);
    let hours = Bodies.rectangle(450,300,100,50,{ isStatic: true}); 
    World.add(engine.world,[seconds, minutes, hours]);
    engine.set = 0;

    return {seconds, minutes, hours};
}